package squelette;

import com.jogamp.opengl.GL2;
import com.jogamp.opengl.glu.GLU;
import forme.Forme;
import forme.Pave;
import forme.Point;
import forme.Voiture;
import forme.route.Route;

import java.awt.event.KeyEvent;
import java.util.List;

/**
 * Monde est la classe princpiale, elle contient tout ce qui est nécessaire à la modélisation de l'environnement.
 * Monde (comme la plupart des autres classes) hérite de la classe Forme, celle-ci étant très importante au
 * fonctionnement du programme je vous invite à lire la documentation avant de continuer.
 */
public class Monde extends Forme {
    private Camera camera;
    private Voiture voiture;
    private Route route;
    private List<Route> routes;

    private boolean freeCamera;
    private boolean thirdPerson;

    /**
     * Constructeur par "défaut" de Monde, toutes les classes héritent de Forme, elles ont besoin de GL2 et d'une
     * position, ici on met par défaut la position au Point (0, 0, 0) qui servira d'origine
     *
     * @param gl
     */
    public Monde(GL2 gl) {
        this(gl, new Point(0, 0, 0));
    }

    /**
     * Constructeur de Monde, il va initialiser les élements contenu dans l'environnement
     *
     * @param gl
     * @param position
     */
    public Monde(GL2 gl, Point position) {
        super(gl, position);
        // On initialise les variables non objet
        int nbPoint = 60;  // Sert à définir le nombre de point à mettre dans les cercles par exemple
        freeCamera = false;  // Par défaut la caméra n'est pas libre
        thirdPerson = true;  // Par défaut la caméra sera en 3ème personne

        // On initialise la voiture
        voiture = new Voiture(gl, position, 25, 10, nbPoint);

        // On initialise la route
        routes = new Route(gl, position, 50).genRoute(300);

        // On initialise la caméra
        camera = new Camera(position.decale(100, 100, 100));
    }

    /**
     * Surcharge de la méthode display, on va ici gérer la caméra et afficher les éléments de l'environnement
     *
     * @param glu
     */
    public void display(GLU glu) {
        // Gestion de la caméra
        if (thirdPerson) {
            camera.lookAt(glu, voiture.getThirdPersonTarget());
            if (voiture.getAvancer()) {
                double x = -Math.cos(Math.toRadians(-voiture.getAngleOrientation()));
                double z = -Math.sin(Math.toRadians(-voiture.getAngleOrientation()));
                camera.setPosition(camera.getPosition().decale(x, 0, z));
            } else if (voiture.getReculer()) {
                double x = Math.cos(Math.toRadians(-voiture.getAngleOrientation()));
                double z = Math.sin(Math.toRadians(-voiture.getAngleOrientation()));
                camera.setPosition(camera.getPosition().decale(x, 0, z));
            }
            if (!freeCamera)
                camera.setPosition(voiture.getThirdPersonPosition());
        } else {
            camera.lookAt(glu, voiture.getFirstPersonTarget());
            if (!freeCamera)
                camera.setPosition(voiture.getFirstPersonPosition());
        }
        // Fin gestion caméra

        // On affiche la voiture
        voiture.display();

        // On affiche chaque route
        for (Route r : routes)
            r.display();
    }

    /**
     * keyPressed est utilisé pour gérer les touches presser au clavier
     *
     * @param e
     */
    public void keyPressed(KeyEvent e) {
        /* En premier lieu on envoi l'event e à la caméra, si celle-ci n'est pas concerné par la touche elle renvoi
         false */
        if (!camera.keyPressed(e)) {
            // Si la touche C est pressé, alors on dit que la caméra est libre ou non
            if (e.getKeyCode() == KeyEvent.VK_C && thirdPerson) {
                freeCamera = !freeCamera;
            } // Sinon si P est pressé alors on passe de la vue 1ère personne à la vue 3ème personne
            else if (e.getKeyCode() == KeyEvent.VK_P && !freeCamera) {
                thirdPerson = !thirdPerson;
            } // Sinon on envoi l'événement à la voiture
            else if (!voiture.keyPressed(e)) {
            }
        }
    }

    /**
     * keyReleased est utilisé pour gérer les touches relacher du clavier
     * @param e
     */
    public void keyReleased(KeyEvent e) {
        // On envoi l'événement à la voiture
        voiture.keyReleased(e);
    }
}
