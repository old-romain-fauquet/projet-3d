package squelette;

import com.jogamp.opengl.GL2;
import com.jogamp.opengl.GLAutoDrawable;
import com.jogamp.opengl.GLEventListener;
import com.jogamp.opengl.glu.GLU;
import com.jogamp.opengl.util.gl2.GLUT;
import forme.*;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

/**
 * Nous avons modifié cette classe afin qu'elle puisse être utiliser comme KeyListener, ce qui nous permet de récupérer
 * les touches pressé du clavier.
 * MyGLEventListener va aussi contenir un objet Monde, qui contient tout ce qui est nécessaire à la modélisation de
 * la voiture et tout ce que l'on voit, en fait la classe MyGLEventListener ne sert que de passerelle entre la classe
 * Monde et l'écran, que ce soit pour l'affichage ou pour les saisie clavier
 */
public class MyGLEventListener implements GLEventListener, KeyListener {
    Monde monde;  // Objet monde

    @Override
    public void init(GLAutoDrawable drawable) {
        GL2 gl = drawable.getGL().getGL2();
        gl.glClearColor(1.0f, 1.0f, 1.0f, 0.0f);

        gl.glEnable(GL2.GL_DEPTH_TEST);
        gl.glClearDepth(100.0f);

        gl.glShadeModel(GL2.GL_SMOOTH);
        gl.glEnable(GL2.GL_LIGHTING);

        gl.glEnable(GL2.GL_LIGHT0);
        gl.glLightfv(GL2.GL_LIGHT0, GL2.GL_AMBIENT, light_0_ambient, 0);
        gl.glLightfv(GL2.GL_LIGHT0, GL2.GL_DIFFUSE, light_0_diffuse, 0);
        gl.glLightfv(GL2.GL_LIGHT0, GL2.GL_SPECULAR, light_0_specular, 0);
        gl.glLightfv(GL2.GL_LIGHT0, GL2.GL_POSITION, light_0_position, 0);

        gl.glEnable(GL2.GL_COLOR_MATERIAL);
        gl.glColorMaterial(GL2.GL_FRONT_AND_BACK, GL2.GL_AMBIENT_AND_DIFFUSE);
        gl.glMateriali(GL2.GL_FRONT_AND_BACK, GL2.GL_SHININESS, 90);

        gl.glMaterialfv(GL2.GL_FRONT_AND_BACK, GL2.GL_SPECULAR, material_specular, 0);

        monde = new Monde(gl); // On initialise ici l'objet monde, il se charge d'initialiser tout le reste
    }


    @Override
    public void dispose(GLAutoDrawable drawable) {

        // TODO Auto-generated method stub
    }


    @Override
    public void display(GLAutoDrawable drawable) {

        GL2 gl = drawable.getGL().getGL2();
        gl.glClear(GL2.GL_COLOR_BUFFER_BIT | GL2.GL_DEPTH_BUFFER_BIT);
        gl.glLoadIdentity();

        monde.display(glu); // Nous executons la méthode display qui va faire un appel récursif des méthodes display
        // des objets que monde contient
    }

    @Override
    public void reshape(GLAutoDrawable drawable, int x, int y, int width, int height) {

        GL2 gl = drawable.getGL().getGL2();
        gl.glViewport(x, y, width, height);

        gl.glMatrixMode(GL2.GL_PROJECTION);
        gl.glLoadIdentity();
        glu.gluPerspective(60, (float) width / height, 0.1f, 10000.0f);
        gl.glMatrixMode(GL2.GL_MODELVIEW);
    }

    private GLU glu = new GLU();
    private GLUT glut = new GLUT();

    private float camera[] = {0f, -50f, 100f};
    private float[] light_0_ambient = {0.5f, 0.5f, 0.5f, 0.0f};
    private float[] light_0_diffuse = {0.75f, 0.75f, 0.75f, 0.0f};
    private float[] light_0_specular = {0.1f, 0.1f, 0.1f, 1.0f};
    private float[] light_0_position = {100f, 0f, 100f, 1f};
    private float[] material_specular = {1.0f, 1.0f, 1.0f, 1.0f};
    private int angle = 0;

    @Override
    public void keyTyped(KeyEvent e) {

    }

    /**
     * keyPressed est une méthode implémenter par KeyListener, on envoi directement le KeyEvent à monde qui traitera
     * comme il faut l'événement.
     * Monde n'implémente pas KeyListener mais pour un soucis de lisibilité du code les méthodes sont nommées de la
     * même manières.
     *
     * @param e
     */
    @Override
    public void keyPressed(KeyEvent e) {
        monde.keyPressed(e);
    }

    /**
     * keyReleased est une méthode implémenter par KeyListener, on envoi directement le KeyEvent à monde qui traitera
     * comme il faut l'événement.
     * Monde n'implémente pas KeyListener mais pour un soucis de lisibilité du code les méthodes sont nommées de la
     * même manières.
     *
     * @param e
     */
    @Override
    public void keyReleased(KeyEvent e) {
        monde.keyReleased(e);
    }
}