package squelette;

import com.jogamp.opengl.glu.GLU;
import forme.Point;

import java.awt.event.KeyEvent;
import java.security.Key;

/**
 * Modèle de la caméra
 */
public class Camera {
    private Point position;
    private Point target;

    /**
     * Constructeur de base de la caméra
     *
     * @param position
     */
    public Camera(Point position) {
        this.position = position;
    }

    /**
     * Fait regarder le point target à la caméra depuis sa position
     */
    public void lookAt(GLU glu, Point target) {
        this.target = target;
        glu.gluLookAt(
                position.getX(), position.getY(), position.getZ(),
                target.getX(), target.getY(), target.getZ(),
                0.0f, 1.0f, 0.0f
        );
    }

    /**
     * Redéfini la position de la caméra
     */
    public void setPosition(Point position) {
        this.position = position;
    }

    /**
     * Retourne la position de la caméra
     *
     * @return Point
     */
    public Point getPosition() {
        return position;
    }

    /**
     * Retourne l'objectif de la caméra
     *
     * @return Point
     */
    public Point getTarget() {
        return target;
    }

    /**
     * Gestion du clavier
     * true si interaction
     *
     * @return boolean
     */
    public boolean keyPressed(KeyEvent e) {
        // La caméra a un comportement assez aléatoire en mode libre car ici les axes bouger sont fixes
        // Pour palier ce problème il faudrait faire un calcul de coefficient des axes comme pour la caméra voiture
        // Nous n'avons malheuresement pas eu le temps de nous pencher sur le problème
        if (e.getKeyCode() == KeyEvent.VK_UP) {
            // Si Haut alors on monte la caméra
            position = position.decale(0, 1, 0);
            return true;
        } else if (e.getKeyCode() == KeyEvent.VK_DOWN) {
            // Si Bas alors on descend la caméra
            position = position.decale(0, -1, 0);
            return true;
        } else if (e.getKeyCode() == KeyEvent.VK_LEFT) {
            // Si Gauche alors on déplace la caméra à gauche
            position = position.decale(-1, 0, 0);
            return true;
        } else if (e.getKeyCode() == KeyEvent.VK_RIGHT) {
            // Si Droite alors on déplace la caméra à droite
            position = position.decale(1, 0, 0);
            return true;
        } else if (e.getKeyCode() == KeyEvent.VK_PAGE_UP) {
            // Si Page Haut alors on approche la caméra
            position = position.decale(0, 0, -1);
            return true;
        } else if (e.getKeyCode() == KeyEvent.VK_PAGE_DOWN) {
            // Si Page Bas alors on recul la caméra
            position = position.decale(0, 0, 1);
            return true;
        }
        return false;
    }
}
