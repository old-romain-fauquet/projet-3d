package forme;

import com.jogamp.opengl.GL2;

import java.util.LinkedList;
import java.util.List;

/**
 * Forme est une classe générique qui permet de définir une forme quelconque, elle contient les méthodes nécessaires
 * (qui doivent parfois être surchargé comme display()) afin de modéliser et afficher une forme
 */
public class Forme {
    protected GL2 gl;
    protected Point position;
    protected double color[] = {0, 0, 0};
    protected List<double[]> rotationOrigine;
    protected List<double[]> rotationLuiMeme;
    protected List<Forme> elementToDisplay;
    protected boolean tournerDepuisPositionRotation;
    protected Point positionRotation;

    /**
     * Méthode bloqué
     */
    protected Forme() {
    }

    /**
     * Constructeur basique d'une forme, initialise la liste d'élément, et affecte la position ainsi que gl
     *
     * @param gl
     * @param position
     */
    public Forme(GL2 gl, Point position) {
        this.gl = gl;
        this.position = position;
        this.elementToDisplay = new LinkedList<>();
        this.rotationLuiMeme = new LinkedList<>();
        this.rotationOrigine = new LinkedList<>();
        this.tournerDepuisPositionRotation = false;
    }

    /**
     * Retourne la position de la forme
     *
     * @return Point
     */
    public Point getPosition() {
        return position;
    }

    /**
     * Retourne la liste d'élément à afficher
     * Cela implique que les valeurs soient renseigné au préalable, dans le constructeur de la forme héritant de
     * Forme par exemple
     *
     * @return List<Forme>
     */
    public List<Forme> elementToDisplay() {
        return elementToDisplay;
    }

    /**
     * Défini la couleur de la forme en rgb
     *
     * @param r
     * @param g
     * @param b
     */
    public void setColor(double r, double g, double b) {
        color = new double[]{r, g, b};
    }

    /**
     * Défini la rotation de la forme par rapport à l'origine
     *
     * @param angle
     * @param x
     * @param y
     * @param z
     */
    public void setRotationOrigine(double angle, double x, double y, double z) {
        rotationOrigine = new LinkedList<>();
        rotationOrigine.add(new double[]{angle, x, y, z});
    }

    /**
     * Ajoute une rotation de la forme par rapport à l'origine
     *
     * @param angle
     * @param x
     * @param y
     * @param z
     */
    public void addRotationOrigine(double angle, double x, double y, double z) {
        rotationOrigine.add(new double[]{angle, x, y, z});
    }

    /**
     * Défini la rotation de la forme sur elle-même
     *
     * @param angle
     * @param x
     * @param y
     * @param z
     */
    public void setRotationLuiMeme(double angle, double x, double y, double z) {
        rotationLuiMeme = new LinkedList<>();
        rotationLuiMeme.add(new double[]{angle, x, y, z});
    }

    /**
     * Ajoute une rotation de la forme par rapport à elle-même
     *
     * @param angle
     * @param x
     * @param y
     * @param z
     */
    public void addRotationLuiMeme(double angle, double x, double y, double z) {
        rotationLuiMeme.add(new double[]{angle, x, y, z});
    }

    /**
     * Déplace la forme au Point p
     *
     * @param p
     */
    public void moveTo(Point p) {
        position = p;
    }

    /**
     * Déplace la forme avec un décalage de x, y et z
     *
     * @param x
     * @param y
     * @param z
     */
    public void move(double x, double y, double z) {
        moveTo(position.decale(x, y, z));
    }

    public void setPositionRotation(double x, double y, double z) {
        tournerDepuisPositionRotation = true;
        positionRotation = new Point(x, y, z);
    }

    public void setPositionRotation(Point p) {
        tournerDepuisPositionRotation = true;
        positionRotation = p;
    }

    public void clearVariable() {
        rotationLuiMeme.clear();
        rotationOrigine.clear();
        tournerDepuisPositionRotation = false;
    }

    /**
     * Display de base, pour un affichage plus avancé il suffit de surcharger la méthode display()
     */
    public void display() {
        gl.glPushMatrix();
        // On défini la couleur de la forme
        gl.glColor3d(color[0], color[1], color[2]);
        // On effectue une rotation de la forme par rapport à l'origine de la matrice
        for (double[] tab : rotationOrigine)
            gl.glRotated(tab[0], tab[1], tab[2], tab[3]);
        // On déplace l'origine de la matrice à la position de la forme
        gl.glTranslated(position.getX(), position.getY(), position.getZ());
        // On effectue la rotation de la forme sur elle-même
        for (double[] tab : rotationLuiMeme)
            gl.glRotated(tab[0], tab[1], tab[2], tab[3]);
        // Pour chaque forme dans elementToDisplay on affiche celles-ci
        for (Forme f : elementToDisplay())
            f.display();
        gl.glPopMatrix();
        clearVariable();
    }
}
