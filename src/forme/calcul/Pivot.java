package forme.calcul;

import forme.Point;

public class Pivot {
    private double angle;
    private Point position;
    private Point t;
    private Point destination;
    private int sens;

    public static int GAUCHE = -1;
    public static int DROITE = 1;
    private static double PRECISION = 0.001;

    private double R;

    public Pivot(double angle, Point position, Point t, Point destination, int sens) {
        this.angle = angle;
        this.position = position;
        this.t = t;
        this.destination = destination;
        this.sens = sens;
        R = -1;
    }

    private Point calculerR() {
        Point vecteurT = Point.getPointMilieu(position, destination);
        Point perpendiVectT = new Point(-vecteurT.getZ(), vecteurT.getY(), vecteurT.getX());
        double pas = sens;

        double deltaPast = 100;
        Point potentiel = vecteurT.decale(0, 0, 0);
        Point precedent = potentiel.decale(0, 0, 0);
        int i = 0;
        while (deltaPast > PRECISION) {
            if (differenceR(potentiel) <= PRECISION)
                return potentiel;
            if (fourchette(deltaPast, differenceR(potentiel))) {
                potentiel = potentiel.decale(
                        -perpendiVectT.getX() / pas,
                        -perpendiVectT.getY() / pas,
                        -perpendiVectT.getZ() / pas);
                pas = pas * 10;
            } else
                potentiel = potentiel.decale(
                        perpendiVectT.getX() / pas,
                        perpendiVectT.getY() / pas,
                        perpendiVectT.getZ() / pas);
            deltaPast = differenceR(potentiel);
        }
        return potentiel;
    }

    private double differenceR(Point alpha) {
        double a = Math.pow(position.getX() - alpha.getX(), 2) + Math.pow(position.getY() - alpha.getY(), 2);
        double b = Math.pow(destination.getX() - alpha.getX(), 2) + Math.pow(destination.getY() - alpha.getY(), 2);
        return abs(a - b);
    }

    private boolean fourchette(double avant, double actuel) {
        return PRECISION >= avant;
    }

    private double abs(double a) {
        if (a < 0)
            return -a;
        return a;
    }

    public double getR() {
        if (R == -1)
            calculerR();
        return R;
    }
}
