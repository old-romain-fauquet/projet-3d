package forme;

import com.jogamp.opengl.GL2;

/**
 * Modélistion d'un pave
 */
public class Pave extends Forme {

    public Pave(GL2 gl, Point position, double longueur, double largeur, double hauteur) {
        super(gl, position);

        Point p1, p2, p3, p4, p5, p6, p7, p8;

        p1 = position.decale(longueur / 2, hauteur / 2, largeur / 2);
        p2 = position.decale(-longueur / 2, hauteur / 2, largeur / 2);
        p3 = position.decale(longueur / 2, -hauteur / 2, largeur / 2);
        p4 = position.decale(-longueur / 2, -hauteur / 2, largeur / 2);

        p5 = position.decale(longueur / 2, hauteur / 2, -largeur / 2);
        p6 = position.decale(-longueur / 2, hauteur / 2, -largeur / 2);
        p7 = position.decale(longueur / 2, -hauteur / 2, -largeur / 2);
        p8 = position.decale(-longueur / 2, -hauteur / 2, -largeur / 2);

        elementToDisplay.add(new Rectangle(gl, p1, p3, p4, p2));
        elementToDisplay.add(new Rectangle(gl, p5, p7, p8, p6));
        elementToDisplay.add(new Rectangle(gl, p1, p5, p7, p3));
        elementToDisplay.add(new Rectangle(gl, p2, p6, p8, p4));
        elementToDisplay.add(new Rectangle(gl, p1, p2, p6, p5));
        elementToDisplay.add(new Rectangle(gl, p3, p7, p8, p4));

    }
}
