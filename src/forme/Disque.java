package forme;

import com.jogamp.opengl.GL2;

import java.util.LinkedList;
import java.util.List;

/**
 * Modélisation d'un disque
 */
public class Disque extends Forme {

    private Cercle contour;
    private List<Triangle> interieur;

    /**
     * Constructeur de base d'un Disque
     *
     * @param gl
     * @param position
     * @param rayon
     * @param nbPoint
     */
    public Disque(GL2 gl, Point position, float rayon, int nbPoint) {
        super(gl, position);
        // On créé le contour du disque
        this.contour = new Cercle(gl, position, rayon, nbPoint);
        // On initialise la liste de triangle
        this.interieur = new LinkedList<>();

        // Pour chaque couples de point de la liste de point du cercle on va créer un triangle entre ce couple et le
        // centre du disque afin de le remplir
        for (int i = 0; i < contour.getPoints().length - 1; i++)
            interieur.add(new Triangle(gl, contour.getPoints()[i], contour.getPoints()[i + 1], position));
        // Puis on ferme en faisant un triangle entre le premier et le dernier point de la liste du cercle
        interieur.add(new Triangle(gl, contour.getPoints()[0], contour.getPoints()[contour.getPoints().length - 1], position));
    }

    /**
     * Retourne l'intérieur du disque
     *
     * @return List<Triangle>
     */
    public List<Triangle> getInterieur() {
        return interieur;
    }

    /**
     * Retourne le cercle du disque
     *
     * @return Cercle
     */
    public Cercle getContour() {
        return contour;
    }

    /**
     * Surcharge de la méthode display
     */
    public void display() {
        // Pour chaque triangle on l'affiche
        gl.glColor3d(color[0], color[1], color[2]);
        for (Triangle triangle : this.interieur)
            triangle.display();
    }

    public double getPerimetre() {
        return contour.getPerimetre();
    }
}
