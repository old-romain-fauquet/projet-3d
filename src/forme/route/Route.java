package forme.route;

import com.jogamp.opengl.GL2;
import forme.Forme;
import forme.Point;

import java.util.LinkedList;
import java.util.List;
import java.util.Random;

public class Route extends Forme {
    private List<Route> liste;
    protected double largeur;

    public Route(GL2 gl, Point position, double largeur) {
        super(gl, position);
        liste = new LinkedList<>();
        this.largeur = largeur;
    }

    public Point getNextPosition() {
        return null;
    }

    public Route randomRoute() {
        Point nextPos = this.position;
        if (liste.size() > 0) {
            nextPos = liste.get(liste.size() - 1).getNextPosition();
        }
        return new Droite(gl, nextPos, largeur);
    }

    public List<Route> genRoute(int nbRoute) {
        for (int i = 0; i < nbRoute; i++)
            liste.add(randomRoute());
        return liste;
    }
}
