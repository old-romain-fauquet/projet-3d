package forme.route;

import com.jogamp.opengl.GL2;
import forme.Pave;
import forme.Point;
import forme.Rectangle;

import java.util.Random;

public class Droite extends Route {
    private Pave base;

    public Droite(GL2 gl, Point position, double largeur) {
        super(gl, position, largeur);

        base = new Pave(gl, position, largeur, largeur, 0);

        base.setColor(0.5, 0.5, 0.5);
        elementToDisplay.add(base);
    }

    public Point getNextPosition() {
        Point p = null;
        switch (new Random().nextInt(3)) {
            case 0:
                p = position.decale(0, 0, largeur / 4);
                break;
            case 1:
                p = position.decale(0, 0, -largeur / 4);
                break;
            case 2:
                p = position.decale(-largeur / 4, 0, 0);
                break;
        }
        return p;
    }

    @Override
    public void display() {
        super.display();
    }
}
