package forme;

/**
 * Modélisation d'une ligne
 */
public class Line {
    private Point p1;
    private Point p2;

    /**
     * Constructeur de base d'une ligne
     *
     * @param p1
     * @param p2
     */
    public Line(Point p1, Point p2) {
        this.p1 = p1;
        this.p2 = p2;
    }

    /**
     * Renvoi le point 1
     *
     * @return Point
     */
    public Point getP1() {
        return p1;
    }

    /**
     * Renvoi le point 2
     *
     * @return Point
     */
    public Point getP2() {
        return p2;
    }

    /**
     * Renvoi le point 1 et 2 sous forme de tableau
     *
     * @return
     */
    public Point[] getCoordonnees() {
        return new Point[]{p1, p2};
    }
}
