package forme;

import com.jogamp.opengl.GL2;

import java.util.LinkedList;
import java.util.List;

/**
 * Rerésentation d'un Cercle
 */
public class Cercle extends Forme {
    private List<Line> ligne;
    private Point[] points;
    private double perimetre;
    private double rayon;

    /**
     * Constructeur basique d'un cercle
     *
     * @param gl
     * @param position
     * @param rayon
     * @param nbPoint
     */
    public Cercle(GL2 gl, Point position, float rayon, int nbPoint) {
        this(gl, position, rayon, nbPoint, 360);
    }

    public Cercle(GL2 gl, Point position, float rayon, int nbPoint, double angleDegree) {
        super(gl, position);
        this.points = new Point[nbPoint];
        this.ligne = new LinkedList<>();
        this.rayon = rayon;
        this.perimetre = 2 * Math.PI * rayon;

        double angle = Math.toRadians(angleDegree);

        // Parcours un tour entier afin de modéliser un cercle
        double theta = 0.0;
        for (int i = 0; i < nbPoint; i++) {
            theta += angle / nbPoint; // Calcul du pas en fonction de nbPoint
            double y = rayon * Math.sin(theta) + position.getY();
            double x = rayon * Math.cos(theta) + position.getX();
            points[i] = new Point((float) x, (float) y, position.getZ()); // Ajout d'un point
        }

        // Pour chaque couple de point on créer une jointure entre eux
        for (int i = 0; i < points.length - 1; i++)
            ligne.add(new Line(points[i], points[i + 1]));

        // On joint le premier et le dernier point de la liste de point
        ligne.add(new Line(points[0], points[points.length - 1]));
    }

    /**
     * Surcharge de la méthode display
     */
    public void display() {
        gl.glBegin(gl.GL_LINES);
        // Pout chaque ligne on parcours les points, puis pour chaque point on trace une ligne entre eux
        for (Line l : ligne)
            for (Point p : l.getCoordonnees())
                gl.glVertex3f(p.getX(), p.getY(), p.getZ());
        gl.glEnd();
    }

    /**
     * Retourne la liste des points
     *
     * @return Point[]
     */
    public Point[] getPoints() {
        return points;
    }

    /**
     * Retourne la position du cercle
     *
     * @return Point
     */
    public Point getPosition() {
        return position;
    }

    public double getPerimetre() {
        return perimetre;
    }
}
