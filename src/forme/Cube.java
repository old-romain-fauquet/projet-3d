package forme;

import com.jogamp.opengl.GL2;

/**
 * Modélisation d'un Cube
 */
public class Cube extends Forme {
    private Quads[] quads;

    /**
     * Constructeur de base d'un cube
     * @param gl
     * @param position
     * @param longueur
     */
    public Cube(GL2 gl, Point position, float longueur) {
        super(gl, position);
        this.quads = new Quads[6];

        // Génération des 8 sommets du Cube
        Point p1, p2, p3, p4, p5, p6, p7, p8;

        p1 = new Point(position.getX() + longueur / 2, position.getY() + longueur / 2, position.getZ() + longueur / 2);
        p2 = new Point(position.getX() - longueur / 2, position.getY() + longueur / 2, position.getZ() + longueur / 2);
        p3 = new Point(position.getX() + longueur / 2, position.getY() - longueur / 2, position.getZ() + longueur / 2);
        p4 = new Point(position.getX() - longueur / 2, position.getY() - longueur / 2, position.getZ() + longueur / 2);

        p5 = new Point(position.getX() + longueur / 2, position.getY() + longueur / 2, position.getZ() - longueur / 2);
        p6 = new Point(position.getX() - longueur / 2, position.getY() + longueur / 2, position.getZ() - longueur / 2);
        p7 = new Point(position.getX() + longueur / 2, position.getY() - longueur / 2, position.getZ() - longueur / 2);
        p8 = new Point(position.getX() - longueur / 2, position.getY() - longueur / 2, position.getZ() - longueur / 2);

        // On créé les 6 faces du cube avec les sommets précédemment créés
        Point[] p = new Point[4];
        p[0] = p1;
        p[1] = p2;
        p[3] = p3;
        p[2] = p4;
        quads[0] = new Quads(p);

        p = new Point[4];
        p[0] = p5;
        p[1] = p6;
        p[3] = p7;
        p[2] = p8;
        quads[1] = new Quads(p);

        p = new Point[4];
        p[0] = p1;
        p[1] = p3;
        p[3] = p5;
        p[2] = p7;
        quads[2] = new Quads(p);

        p = new Point[4];
        p[0] = p2;
        p[1] = p4;
        p[3] = p6;
        p[2] = p8;
        quads[3] = new Quads(p);

        p = new Point[4];
        p[0] = p1;
        p[1] = p5;
        p[3] = p2;
        p[2] = p6;
        quads[4] = new Quads(p);

        p = new Point[4];
        p[0] = p3;
        p[1] = p7;
        p[3] = p4;
        p[2] = p8;
        quads[5] = new Quads(p);
    }

    /**
     * Surcharge de la méthode display()
     */
    public void display() {
        gl.glBegin(gl.GL_QUADS);
        // Pour chaque face on les affiches
        for (Quads q : quads)
            for (float[] f : q.getCoordonnees())
                gl.glVertex3f(f[0], f[1], f[2]);
        gl.glEnd();
    }
}
