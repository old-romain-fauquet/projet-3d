package forme;

import com.jogamp.opengl.GL2;

/**
 * Modèle de Coque
 */
public class Coque extends Forme {
    private Pave base;
    private Pave top;
    private Pave retroD;
    private Pave retroG;
    private Pave bout;
    private Pave plaque;
    private Pave vitreAvant;
    private Pave vitreArriere;
    private Pave vitreAvG;
    private Pave vitreAvD;
    private Pave vitreArrG;
    private Pave vitreArrD;
    private Pave antenne;

    /**
     * Constructeur de base de Coque
     *
     * @param gl
     * @param position
     * @param longueur
     * @param largeur
     */
    public Coque(GL2 gl, Point position, double longueur, double largeur) {
        super(gl, position);

        // On défini les formes de la coque
        base = new Pave(gl, position, longueur, largeur, 5);
        top = new Pave(gl, position.decale(0,2,0),longueur/2, largeur, 7);
        retroD = new Pave(gl,position.decale(-5,2,-largeur/4),1, 4, 2);
        retroG = new Pave(gl,position.decale(-5,2,largeur/4),1,4,2);
        bout = new Pave(gl,position.decale(-longueur/4+1,1.5,0),1,1,1);
        plaque = new Pave(gl,position.decale(longueur/4,0,0),1,6,1.5);
        vitreAvant = new Pave(gl,position.decale(-longueur/8,2.5,0),1,largeur-2,3);
        vitreArriere = new Pave(gl,position.decale(longueur/8,2.5,0),1,largeur-4,3);
        vitreAvD = new Pave(gl,position.decale(-3,2.5,-largeur/4),6,1,3);
        vitreAvG = new Pave(gl,position.decale(-3,2.5,largeur/4),6,1,3);
        vitreArrD = new Pave(gl,position.decale(2,2.5,-largeur/4),6,1,3);
        vitreArrG = new Pave(gl,position.decale(2,2.5,largeur/4),6,1,3);
        antenne = new Pave(gl,position.decale(-longueur/9,4,0),0.3,0.3,4);
        // On ajoute les éléments à afficher
        elementToDisplay.add(base);
        elementToDisplay.add(top);
        elementToDisplay.add(retroD);
        elementToDisplay.add(retroG);
        elementToDisplay.add(bout);
        elementToDisplay.add(plaque);
        elementToDisplay.add(vitreAvant);
        elementToDisplay.add(vitreArriere);
        elementToDisplay.add(vitreAvD);
        elementToDisplay.add(vitreAvG);
        elementToDisplay.add(vitreArrD);
        elementToDisplay.add(vitreArrG);
        elementToDisplay.add(antenne);

        // On défini la rotation de la coque
        base.setRotationLuiMeme(90, 0, 1, 0);
        top.setRotationLuiMeme(90,0,1,0);
    }

    /**
     * Surcharge de la méthodes display()
     */
    public void display() {
        super.display();
    }

    /**
     * Surcharge de la méthode setColor
     */
    public void setColor(double r, double g, double b) {
        super.setColor(r, g, b);
        // Pour chaque éléments de la coque définit la couleur
        for (Forme f : elementToDisplay) {
            f.setColor(r, g, b);
        }

        elementToDisplay.get(elementToDisplay.size()-1).setColor(0.1 ,0.1 ,0.1 );
        elementToDisplay.get(elementToDisplay.size()-2).setColor(0.8 ,0.8 ,0.8 );
        elementToDisplay.get(elementToDisplay.size()-3).setColor(0.8 ,0.8 ,0.8 );
        elementToDisplay.get(elementToDisplay.size()-4).setColor(0.8 ,0.8 ,0.8 );
        elementToDisplay.get(elementToDisplay.size()-5).setColor(0.8 ,0.8 ,0.8 );
        elementToDisplay.get(elementToDisplay.size()-6).setColor(0.8 ,0.8 ,0.8 );
        elementToDisplay.get(elementToDisplay.size()-7).setColor(0.8 ,0.8 ,0.8 );
        elementToDisplay.get(elementToDisplay.size()-8).setColor(0.658824 ,0.658824 ,0.658824 );
        elementToDisplay.get(elementToDisplay.size()-9).setColor(0.658824 ,0.658824 ,0.658824 );
        elementToDisplay.get(elementToDisplay.size()-10).setColor(0.658824 ,0.658824 ,0.658824 );
        elementToDisplay.get(elementToDisplay.size()-11).setColor(0.658824 ,0.658824 ,0.658824 );
    }
}
