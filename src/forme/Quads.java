package forme;

public class Quads {
    private Point[] points;

    public Quads() {
        points = new Point[4];

        points[0] = new Point(1, 0, 0);
        points[1] = new Point(0, 1, 0);
        points[2] = new Point(0, 0, 1);
        points[3] = new Point(2, 0, 0);
    }

    public Quads(Point[] points) {
        this.points = points;
    }

    public Point[] getPoints() {
        return points;
    }

    public float[][] getCoordonnees() {
        float[][] coordonnees = new float[points.length][];
        int i = 0;
        for (Point p : points) {
            coordonnees[i] = p.toTab();
            i++;
        }
        return coordonnees;
    }
}
