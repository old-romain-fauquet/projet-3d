package forme;

import com.jogamp.opengl.GL2;

public class Triangle {
    private GL2 gl;
    private Point p1;
    private Point p2;
    private Point p3;

    public Triangle(GL2 gl, Point p1, Point p2, Point p3) {
        this.gl = gl;
        this.p1 = p1;
        this.p2 = p2;
        this.p3 = p3;
    }

    public void display() {
        gl.glBegin(gl.GL_TRIANGLES);
        gl.glVertex3f(p1.getX(), p1.getY(), p1.getZ());
        gl.glVertex3f(p2.getX(), p2.getY(), p2.getZ());
        gl.glVertex3f(p3.getX(), p3.getY(), p3.getZ());
        gl.glEnd();
    }
}
