package forme;

import com.jogamp.opengl.GL2;

import java.util.LinkedList;
import java.util.List;

/**
 * Modélisation d'un Cylindre
 */
public class Cylindre extends Forme {

    private Disque haut;
    private Disque bas;
    private List<Rectangle> contour;

    /**
     * Constructeur de Cylindre ayant en paramètre des doubles, uniquement par soucis de simplification du développement
     *
     * @param gl
     * @param position
     * @param rayon
     * @param hauteur
     * @param nbPoint
     */
    public Cylindre(GL2 gl, Point position, double rayon, double hauteur, int nbPoint) {
        this(gl, position, (float) rayon, (float) hauteur, nbPoint);
    }

    /**
     * Cosntructeur de base de Cylindre
     *
     * @param gl
     * @param position
     * @param rayon
     * @param hauteur
     * @param nbPoint
     */
    public Cylindre(GL2 gl, Point position, float rayon, float hauteur, int nbPoint) {
        super(gl, position);
        // On initialise le contour du cylindre
        this.contour = new LinkedList<>();

        // On initialise les disque du haut et de bas du Cylindre
        this.haut = new Disque(gl, position.decale(0, 0, hauteur / 2), rayon, nbPoint);
        this.bas = new Disque(gl, position.decale(0, 0, -1 * (hauteur / 2)), rayon, nbPoint);

        // Pour chaque Point des contours des disques haut/bas on génére un rectangle afin de cercler le Cylindre
        for (int i = 0; i < haut.getContour().getPoints().length - 2; i += 1)
            contour.add(new Rectangle(gl,
                    haut.getContour().getPoints()[i],
                    bas.getContour().getPoints()[i],
                    bas.getContour().getPoints()[i + 1],
                    haut.getContour().getPoints()[i + 1]));
        // On créer un dernier rectangle entre les permiers et derniers point de la liste de point des diques
        contour.add(new Rectangle(gl,
                haut.getContour().getPoints()[0],
                bas.getContour().getPoints()[0],
                bas.getContour().getPoints()[bas.getContour().getPoints().length - 1],
                haut.getContour().getPoints()[haut.getContour().getPoints().length - 1]));
    }

    /**
     * Surcharge de la méthode display
     */
    public void display() {
        // On affiche le disque du haut et du bas
        haut.display();
        bas.display();
        // Pour chaque Rectangle du contour on l'affiche
        for (Rectangle rectangle : contour)
            rectangle.display();
    }

    public double getPerimetre() {
        return haut.getPerimetre();
    }
}
