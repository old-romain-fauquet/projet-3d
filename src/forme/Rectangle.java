package forme;

import com.jogamp.opengl.GL2;


public class Rectangle extends Forme {
    private Point p1;
    private Point p2;
    private Point p3;
    private Point p4;

    public Rectangle(GL2 gl, Point p1, Point p2, Point p3, Point p4) {
        super(gl, new Point(0, 0, 0));
        this.p1 = p1;
        this.p2 = p2;
        this.p3 = p3;
        this.p4 = p4;
    }

    public void display() {
        gl.glBegin(gl.GL_QUADS);
        gl.glVertex3f(p1.getX(), p1.getY(), p1.getZ());
        gl.glVertex3f(p2.getX(), p2.getY(), p2.getZ());
        gl.glVertex3f(p3.getX(), p3.getY(), p3.getZ());
        gl.glVertex3f(p4.getX(), p4.getY(), p4.getZ());
        gl.glEnd();
    }
}
