package forme;

public class Point {
    private float x;
    private float y;
    private float z;

    public Point(double x, double y, double z) {
        this((float) x, (float) y, (float) z);
    }

    public Point(float x, float y, float z) {
        this.x = x;
        this.y = y;
        this.z = z;
    }

    public float getX() {
        return x;
    }

    public float getY() {
        return y;
    }

    public float getZ() {
        return z;
    }

    public Point decale(double x, double y, double z) {
        return decale((float) x, (float) y, (float) z);
    }


    public Point decale(float x, float y, float z) {
        return new Point(this.x + x, this.y + y, this.z + z);
    }

    public float[] toTab() {
        return new float[]{x, y, z};
    }

    public static Point getPointMilieu(Point a, Point b) {
        return new Point(
                (a.getX() - b.getX()) / 2,
                (a.getY() - b.getY()) / 2,
                (a.getZ() - b.getZ()) / 2);

    }
}
