package forme;

import com.jogamp.opengl.GL2;

import java.awt.event.KeyEvent;

/**
 * Modélisation d'une voiture
 */
public class Voiture extends Forme {
    private Roue roueAvantDroit;
    private Roue roueAvantGauche;
    private Roue roueArriereGauche;
    private Roue roueArriereDroit;

    private Coque coque;  // Coque de la voiture

    private Point thirdPersonPosition;  // Position de la caméra vue 3ème personne
    private Point thirdPersonTarget;    // Position de l'objectif de la caméra 3ème personne
    private Point firstPersonPosition;  // Position de la caméra vue 1ère personne
    private Point firstPersonTarget;    // Position de l'objectif de la caméra 1ère personne

    private double angleRotationRoue;
    private double angleOrientation; // Angle de pivot de la voiture sur l'axe Y

    // boolean qui permette la gestion du clavier, afin de dire si on avance / recule et si on tourne à droite / gauche
    private boolean avancer;
    private boolean reculer;
    private boolean tournerGauche;
    private boolean tournerDroite;

    private double longueur;


    /**
     * Constructeur de base d'une voiture, va initialiser les roues et les coques en fonction de la position, la
     * longueur et la largeur.
     * Le paramètre nbPoint permet de définir le nombre de point à afficher dans les courbes/cercles
     *
     * @param gl
     * @param position
     * @param longueur
     * @param largeur
     * @param nbPoint
     */
    public Voiture(GL2 gl, Point position, double longueur, double largeur, int nbPoint) {
        super(gl, position);
        this.longueur = longueur;
        double rayon = largeur / 2.5;
        this.position = position.decale(0, rayon, 0);
        double hauteur = (rayon * 4) / 5;

        angleOrientation = 0;
        angleRotationRoue = 0;

        calculCameraView();

        // Initialisation des roues
        roueArriereGauche = new Roue(gl, position.decale(longueur / 2, 0, largeur / 2), rayon, hauteur, nbPoint);
        roueArriereDroit = new Roue(gl, position.decale(longueur / 2, 0, -largeur / 2), rayon, hauteur, nbPoint);
        roueAvantDroit = new Roue(gl, position.decale(-longueur / 2, 0, -largeur / 2), rayon, hauteur, nbPoint);
        roueAvantGauche = new Roue(gl, position.decale(-longueur / 2, 0, largeur / 2), rayon, hauteur, nbPoint);

        // Initialisation de la coque
        coque = new Coque(gl, position.decale(0, rayon / 3, 0), longueur * 1.5, largeur * 1.5);
        coque.setColor(0.9, 0.38, 0.96);  /* met la voiture en rose, à la base on souhaitait reproduire
        la voiture des Simpsons, mais tout ne s'est pas passé comme prévu. Mais on a garder la couleur */

        // On ajoute les roues et la coque à la liste des éléments à afficher
        elementToDisplay.add(roueAvantDroit);
        elementToDisplay.add(roueAvantGauche);
        elementToDisplay.add(roueArriereDroit);
        elementToDisplay.add(roueArriereGauche);
        elementToDisplay.add(coque);
    }

    /**
     * Calcul la position et l'objectif de la caméra à la vue troisième personne
     */
    public void calculThirdPersonCamera() {
        /* on calcul la position de la caméra en la plaçant sur un "cercle" de rayon 70 autour de la voiture, avec
        l'orientation en fonction de celle de la voiture */
        double x = 70 * Math.cos(Math.toRadians(-angleOrientation));
        double z = 70 * Math.sin(Math.toRadians(-angleOrientation));
        thirdPersonPosition = position.decale(x, 30, z);
        thirdPersonTarget = position.decale(0, 0, 0);
    }

    /**
     * Calcul la position et l'objectif à la vue première personne
     */
    public void calculFirstPersonCamera() {
        /* on calcul la position de la caméra en la plaçant sur un "cercle" de rayon 70 autour de la voiture, avec
        l'orientation en fonction de celle de la voiture */
        double x = -100 * Math.cos(Math.toRadians(-angleOrientation));
        double z = -100 * Math.sin(Math.toRadians(-angleOrientation));
        firstPersonPosition = position.decale(0, 20, 0);
        firstPersonTarget = position.decale(x, 0, z);
    }

    /**
     * Calcul la position et l'objectif de la caméra
     */
    public void calculCameraView() {
        calculThirdPersonCamera();
        calculFirstPersonCamera();
    }

    /**
     * Retourne la position de la caméra à la 3ème personne
     *
     * @return Point
     */
    public Point getThirdPersonPosition() {
        return thirdPersonPosition;
    }

    /**
     * Retourne la position de l'objectif à la troisème personne
     *
     * @return Point
     */
    public Point getThirdPersonTarget() {
        return thirdPersonTarget;
    }

    /**
     * Retourne la position de la caméra à la première personne
     *
     * @return Point
     */
    public Point getFirstPersonPosition() {
        return firstPersonPosition;
    }

    /**
     * Retourne la position de l'objectif à la première personne
     *
     * @return Point
     */
    public Point getFirstPersonTarget() {
        return firstPersonTarget;
    }

    /**
     * Retourne si la voiture avance
     *
     * @return boolean
     */
    public boolean getAvancer() {
        return avancer;
    }

    /**
     * Retourne si la voiture recule
     *
     * @return boolean
     */
    public boolean getReculer() {
        return reculer;
    }

    /**
     * Retourne la l'angle d'orientation de la voiture
     *
     * @return double
     */
    public double getAngleOrientation() {
        return angleOrientation;
    }

    /**
     * Display de la voiture, calcul les positions caméra puis effectue le display de Forme
     */
    public void display() {
        gestionEvent();
        calculCameraView();
        super.display();
    }

    /**
     * Fonction qui permet de faire tourner les roues et la voiture, tourne mal, tentative d'amélioration dans la
     * branche pivot_voiture
     * @param angle
     */
    public void tourner(double angle) {
        double angleRoue = 50;
        if (reculer)
            pivoterRoue((angle < 0) ? angleRoue : -angleRoue);
        else
            pivoterRoue((angle < 0) ? -angleRoue : angleRoue);

        this.angleOrientation += angle;
        addRotationLuiMeme(angleOrientation, 0, 1, 0);
    }

    /**
     * Pivote les deux roues dans l'angle donné
     * @param angle
     */
    public void pivoterRoue(double angle) {
        roueAvantDroit.setAnglePivot(angle);
        roueAvantGauche.setAnglePivot(angle);
    }

    /**
     * Tourne les roues à la vitesse calculé
     */
    public void tournerRoues() {
        roueAvantDroit.addRotationLuiMeme(angleRotationRoue, 0, 0, 1);
        roueAvantGauche.addRotationLuiMeme(angleRotationRoue, 0, 0, 1);
        roueArriereDroit.addRotationLuiMeme(angleRotationRoue, 0, 0, 1);
        roueArriereGauche.addRotationLuiMeme(angleRotationRoue, 0, 0, 1);
    }

    /**
     * Retourne l'angle dans lequel les roues devront pivoter pour donner un air réaliste à la rotation des roues
     * @return angle (deg)
     */
    public double getAngleRotationRoue() {
        /* AngleRot = 360  *  ( DistanceParcourue  /  Circonférence )
                       ^        ^                           ^
           1 tour complet       par la voiture
       */
        return 360 * (1 / roueAvantGauche.getPerimetre());
    }

    /**
     * Fait avancer la voiture en fonction de l'angle de la voiture
     */
    public void avancer() {
        double x = -Math.cos(Math.toRadians(-angleOrientation));
        double z = -Math.sin(Math.toRadians(-angleOrientation));
        move(x, 0, z);
        angleRotationRoue += getAngleRotationRoue();
        tournerRoues();
        if (angleRotationRoue > 360) {
            angleRotationRoue = 0;
        }
    }

    /**
     * Fait reculer la voiture en fonction de l'angle de la voiture
     */
    public void reculer() {

        double x = Math.cos(Math.toRadians(-angleOrientation));
        double z = Math.sin(Math.toRadians(-angleOrientation));
        move(x, 0, z);
        angleRotationRoue -= getAngleRotationRoue();
        tournerRoues();
        if (angleRotationRoue < 0) {
            angleRotationRoue = 360;
        }
    }

    /**
     * Déplace la voiture en fonction des touches actuellement pressé
     */
    public void gestionEvent() {
        if (avancer) {
            avancer();
            if (tournerGauche)
                tourner(1.5);
            else if (tournerDroite)
                tourner(-1.5);
            else {
                addRotationLuiMeme(angleOrientation, 0, 1, 0);
                pivoterRoue(0);
            }
        } else if (reculer) {
            reculer();
            if (tournerGauche)
                tourner(-1.5);
            else if (tournerDroite)
                tourner(1.5);
            else {
                addRotationLuiMeme(angleOrientation, 0, 1, 0);
                pivoterRoue(0);
            }
        } else {
            addRotationLuiMeme(angleOrientation, 0, 1, 0);
            tournerRoues();
        }
    }

    /**
     * Gestion pression touche clavier
     */
    public boolean keyPressed(KeyEvent e) {
        if (e.getKeyCode() == KeyEvent.VK_Z) {
            avancer = true;
            return true;
        } else if (e.getKeyCode() == KeyEvent.VK_S) {
            reculer = true;
            return true;
        } else if (e.getKeyCode() == KeyEvent.VK_D) {
            tournerDroite = true;
            return true;
        } else if (e.getKeyCode() == KeyEvent.VK_Q) {
            tournerGauche = true;
            return true;
        }
        return false;
    }

    /**
     * Gestion relachement touche clavier
     */
    public boolean keyReleased(KeyEvent e) {
        if (e.getKeyCode() == KeyEvent.VK_Z) {
            avancer = false;
            return true;
        } else if (e.getKeyCode() == KeyEvent.VK_S) {
            reculer = false;
            return true;
        } else if (e.getKeyCode() == KeyEvent.VK_D) {
            tournerDroite = false;
            return true;
        } else if (e.getKeyCode() == KeyEvent.VK_Q) {
            tournerGauche = false;
            return true;
        }
        return false;
    }
}
