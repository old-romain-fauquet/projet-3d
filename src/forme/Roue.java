package forme;

import com.jogamp.opengl.GL2;


/**
 * Modèlde de Roue
 */
public class Roue extends Forme {
    private Cylindre cylindre;
    private Disque jante1;
    private Disque jante2;
    private double anglePivot;

    /**
     * Constructeur par défaut de Roue
     *
     * @param gl
     * @param position
     * @param rayon
     * @param hauteur
     * @param nbPoint
     */
    public Roue(GL2 gl, Point position, double rayon, double hauteur, int nbPoint) {
        super(gl, position);
        anglePivot = 0;
        // On créé le cylindre de la roue
        cylindre = new Cylindre(gl, new Point(0, 0, 0), rayon, hauteur, nbPoint);

        jante1 = new Disque(gl, new Point(0, 0, hauteur / 2), (float) (rayon * 0.8), 3);
        jante2 = new Disque(gl, new Point(0, 0, -hauteur / 2), (float) (rayon * 0.8), 3);

        jante1.setColor(0.5, 0.5, 0.5);
        jante2.setColor(0.5, 0.5, 0.5);

        // On ajoute les éléments à afficher
        elementToDisplay.add(jante1);
        elementToDisplay.add(jante2);
        elementToDisplay.add(cylindre);
    }

    /**
     * Surcharge de la méthode display()
     */
    public void display() {
        gl.glPushMatrix();
        // On défini la couleur de la forme
        gl.glColor3d(color[0], color[1], color[2]);
        // On effectue une rotation de la forme par rapport à l'origine de la matrice
        for (double[] tab : rotationOrigine)
            gl.glRotated(tab[0], tab[1], tab[2], tab[3]);
        // On déplace l'origine de la matrice à la position de la forme
        gl.glTranslated(position.getX(), position.getY(), position.getZ());
        gl.glRotated(anglePivot, 0, 1, 0);
        // On effectue la rotation de la forme sur elle-même
        for (double[] tab : rotationLuiMeme)
            gl.glRotated(tab[0], tab[1], tab[2], tab[3]);
        gl.glPushMatrix();
        gl.glPopMatrix();
        // Pour chaque forme dans elementToDisplay on affiche celles-ci
        for (Forme f : elementToDisplay())
            f.display();
        gl.glPopMatrix();
        clearVariable();
    }

    public double getPerimetre() {
        return cylindre.getPerimetre();
    }

    public void setAnglePivot(double angle) {
        this.anglePivot = angle;
    }
}
